// INCLUDES
#include "stdio.h"
#include "usbh_core.h"
#include "usbh_msc.h"
#include "ff.h"
#include "ff_gen_drv.h"

// EXTERNS
extern USBH_HandleTypeDef hUSBHost;
extern FATFS USBH_fatfs;

// FUNCTION DEFINITIONS
FRESULT Explore_Disk(char *path, uint8_t recu_level);
void File_Operation(char* title, char* data);
